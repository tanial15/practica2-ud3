package com.tlopez.hospital;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "operaciones", schema = "hospital", catalog = "")
public class Operacion {
    private int id;
    private String sala;
    private Date fecha;
    private int duracion;
    private Paciente paciente;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "sala")
    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    @Basic
    @Column(name = "fecha")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Basic
    @Column(name = "duracion")
    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Operacion operacion = (Operacion) o;
        return id == operacion.id &&
                duracion == operacion.duracion &&
                Objects.equals(sala, operacion.sala) &&
                Objects.equals(fecha, operacion.fecha);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sala, fecha, duracion);
    }

    @ManyToOne
    @JoinColumn(name = "id_paciente", referencedColumnName = "id", nullable = false)
    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Operacion() {
    }

    public Operacion(String sala, Date fecha, int duracion) {
        this.sala = sala;
        this.fecha = fecha;
        this.duracion = duracion;
    }

    @Override
    public String toString() {
        return sala + ", " + fecha;
    }
}
