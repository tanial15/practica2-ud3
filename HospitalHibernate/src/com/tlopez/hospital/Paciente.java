package com.tlopez.hospital;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "pacientes", schema = "hospital", catalog = "")
public class Paciente {
    private int id;
    private String nombre;
    private String apellidos;
    private int edad;
    private Date fechaNac;
    private List<Operacion> operaciones;
    private List<Consulta> consultas;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "edad")
    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Basic
    @Column(name = "fecha_nac")
    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Paciente paciente = (Paciente) o;
        return id == paciente.id &&
                edad == paciente.edad &&
                Objects.equals(nombre, paciente.nombre) &&
                Objects.equals(apellidos, paciente.apellidos) &&
                Objects.equals(fechaNac, paciente.fechaNac);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, edad, fechaNac);
    }

    @OneToMany(mappedBy = "paciente")
    public List<Operacion> getOperaciones() {
        return operaciones;
    }

    public void setOperaciones(List<Operacion> operaciones) {
        this.operaciones = operaciones;
    }

    @OneToMany(mappedBy = "paciente")
    public List<Consulta> getConsultas() {
        return consultas;
    }

    public void setConsultas(List<Consulta> consultas) {
        this.consultas = consultas;
    }

    public Paciente() {
    }

    public Paciente(String nombre, String apellidos, int edad, Date fechaNac) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
        this.fechaNac = fechaNac;
    }

    @Override
    public String toString() {
        return nombre + ", " + apellidos;
    }
}
