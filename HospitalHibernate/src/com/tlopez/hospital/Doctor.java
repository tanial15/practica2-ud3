package com.tlopez.hospital;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "doctores", schema = "hospital", catalog = "")
public class Doctor {
    private int id;
    private String nombre;
    private String apellidos;
    private int numExpediente;
    private Date fechaNac;
    private List<Consulta> consultas;
    private List<Especialidades> especialidades;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "num_expediente")
    public int getNumExpediente() {
        return numExpediente;
    }

    public void setNumExpediente(int numExpediente) {
        this.numExpediente = numExpediente;
    }

    @Basic
    @Column(name = "fecha_nac")
    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Doctor doctor = (Doctor) o;
        return id == doctor.id &&
                numExpediente == doctor.numExpediente &&
                Objects.equals(nombre, doctor.nombre) &&
                Objects.equals(apellidos, doctor.apellidos) &&
                Objects.equals(fechaNac, doctor.fechaNac);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, numExpediente, fechaNac);
    }

    @OneToMany(mappedBy = "doctor")
    public List<Consulta> getConsultas() {
        return consultas;
    }

    public void setConsultas(List<Consulta> consultas) {
        this.consultas = consultas;
    }

    @ManyToMany
    @JoinTable(name = "especialidades_doctores", catalog = "", schema = "hospital", joinColumns =
    @JoinColumn(name = "id_especialidad", referencedColumnName = "id", nullable = false), inverseJoinColumns =
    @JoinColumn(name = "id_doctor", referencedColumnName = "id", nullable = false))
    public List<Especialidades> getEspecialidades() {
        return especialidades;
    }

    public void setEspecialidades(List<Especialidades> especialidades) {
        this.especialidades = especialidades;
    }

    public Doctor() {
    }

    public Doctor(String nombre, String apellidos, int numExpediente, Date fechaNac) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.numExpediente = numExpediente;
        this.fechaNac = fechaNac;
    }

    @Override
    public String toString() {
        return  nombre +", " + apellidos;
    }
}
