package com.tlopez.hospital;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Especialidades {
    private int id;
    private String nombre;
    private int anios;
    private Date fechaTitulacion;
    private List<Doctor> doctores;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "anios")
    public int getAnios() {
        return anios;
    }

    public void setAnios(int anios) {
        this.anios = anios;
    }

    @Basic
    @Column(name = "fecha_titulacion")
    public Date getFechaTitulacion() {
        return fechaTitulacion;
    }

    public void setFechaTitulacion(Date fechaTitulacion) {
        this.fechaTitulacion = fechaTitulacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Especialidades that = (Especialidades) o;
        return id == that.id &&
                anios == that.anios &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(fechaTitulacion, that.fechaTitulacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, anios, fechaTitulacion);
    }

    @ManyToMany(mappedBy = "especialidades")
    public List<Doctor> getDoctores() {
        return doctores;
    }

    public void setDoctores(List<Doctor> doctores) {
        this.doctores = doctores;
    }

    public Especialidades() {
    }

    public Especialidades(String nombre, int anios, Date fechaTitulacion) {
        this.nombre = nombre;
        this.anios = anios;
        this.fechaTitulacion = fechaTitulacion;
    }

    @Override
    public String toString() {
        return  nombre + ", " + anios;
    }

}
