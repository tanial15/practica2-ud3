package com.tlopez.hospital.gui;

import com.tlopez.hospital.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

/**
 * Clase Modelo
 * @author tania
 */
public class Modelo {
    SessionFactory sessionFactory;

    /**
     * Conectar con hibernate
     */
    public void conectar() {
        Configuration configuracion = new Configuration();
        configuracion.configure("hibernate.cfg.xml");

        //Indico la clase mapeada con anotaciones
        configuracion.addAnnotatedClass(Doctor.class);
        configuracion.addAnnotatedClass(Especialidades.class);
        configuracion.addAnnotatedClass(Paciente.class);
        configuracion.addAnnotatedClass(Consulta.class);
        configuracion.addAnnotatedClass(Operacion.class);

        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuracion.buildSessionFactory(ssr);

    }

    /**
     * Desconectar
     */
    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Insertar doctor
     * @param nuevoDoctor
     */
    public void altaDoctor(Doctor nuevoDoctor){
        Session session= sessionFactory.openSession();

        session.beginTransaction();
        //guardo en base de datos
        session.save(nuevoDoctor);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Listar doctor
     * @return lista
     */
    public ArrayList<Doctor> getDoctor(){
        Session session= sessionFactory.openSession();
        Query query=session.createQuery("FROM Doctor");
        ArrayList<Doctor> lista = (ArrayList<Doctor>)query.getResultList();
        session.close();
        return lista;
    }

    /**
     * Modificar doctor
     * @param doctorSeleccion
     */
    public  void  modificarDoctor(Doctor doctorSeleccion){
        Session session= sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(doctorSeleccion);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Eliminar doctor
     * @param doctorBorrado
     */
    public void eliminarDoctor(Doctor doctorBorrado){
        Session session= sessionFactory.openSession();
        session.beginTransaction();
        session.delete(doctorBorrado);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Insertar especialidad
     * @param nuevaEspecialidad
     */
    public void altaEspecialidad(Especialidades nuevaEspecialidad){
        Session session= sessionFactory.openSession();
        session.beginTransaction();
        //guardo en base de datos
        session.save(nuevaEspecialidad);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Listar especialidades
     * @return lista
     */
    public ArrayList<Especialidades> getEspecialidad(){
        Session session= sessionFactory.openSession();
        Query query=session.createQuery("FROM Especialidades");
        ArrayList<Especialidades> lista = (ArrayList<Especialidades>)query.getResultList();
        session.close();
        return lista;
    }

    /**
     * Modificar especialidad
     * @param especialidadSeleccion
     */
    public  void  modificarEspecialidad(Especialidades especialidadSeleccion){
        Session session= sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(especialidadSeleccion);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Eliminar Especialidad
     * @param especialidadBorrado
     */
    public void eliminarEspecialidad(Especialidades especialidadBorrado){
        Session session= sessionFactory.openSession();
        session.beginTransaction();
        session.delete(especialidadBorrado);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Alta de paciente
     * @param nuevoPaciente
     */
    public void altaPaciente(Paciente nuevoPaciente){
        Session session= sessionFactory.openSession();
        session.beginTransaction();
        //guardo en base de datos
        session.save(nuevoPaciente);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Listar pacientes
     * @return lista
     */
    public ArrayList<Paciente> getPaciente(){
        Session session= sessionFactory.openSession();
        Query query=session.createQuery("FROM Paciente");
        ArrayList<Paciente> lista = (ArrayList<Paciente>)query.getResultList();
        session.close();
        return lista;
    }

    /**
     * Modificar paciente
     * @param pacienteSeleccion
     */
    public void  modificarPaciente(Paciente pacienteSeleccion){
        Session session= sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(pacienteSeleccion);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Eliminar paciente
     * @param pacienteBorrado
     */
    public void eliminarPaciente(Paciente pacienteBorrado){
        Session session= sessionFactory.openSession();
        session.beginTransaction();
        session.delete(pacienteBorrado);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Crear Operacion
     * @param nuevaOperacion
     */
    public void altaOperacion(Operacion nuevaOperacion){
        Session session= sessionFactory.openSession();
        session.beginTransaction();
        session.save(nuevaOperacion);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Listar operacion
     * @return lista
     */
    public ArrayList<Operacion> getOperacion(){
        Session session= sessionFactory.openSession();
        Query query=session.createQuery("FROM Operacion");
        ArrayList<Operacion> lista = (ArrayList<Operacion>)query.getResultList();
        session.close();
        return lista;
    }

    /**
     * Modificar operacion
     * @param operacionSeleccion
     */
    public void modificarOperacion(Operacion operacionSeleccion){
        Session session= sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(operacionSeleccion);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Eliminar operacion
     * @param operacionBorrado
     */
    public void eliminarOperacion(Operacion operacionBorrado){
        Session session= sessionFactory.openSession();
        session.beginTransaction();
        session.delete(operacionBorrado);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Lista las operaciones de pacientes
     * @param pacienteSeleccionado
     * @return lista
     */
    public ArrayList<Operacion> getOperacionPaciente(Paciente pacienteSeleccionado){
        Session session= sessionFactory.openSession();
        Query query = session.createQuery("FROM Operacion WHERE Paciente = :prop");
        query.setParameter("prop", pacienteSeleccionado);
        ArrayList<Operacion> lista = (ArrayList<Operacion>) query.getResultList();
        session.close();
        return lista;
    }

    /**
     * Lista los pacientes
     * @return listaPaciente
     */
    public ArrayList<Paciente> getPacientes(){
        Session session= sessionFactory.openSession();
        Query query=session.createQuery("FROM Paciente ");
        ArrayList<Paciente> listaPaciente = (ArrayList<Paciente>)query.getResultList();
        session.close();
        return listaPaciente;
    }
}
