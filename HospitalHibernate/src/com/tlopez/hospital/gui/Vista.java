package com.tlopez.hospital.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.tlopez.hospital.Paciente;

import javax.swing.*;

/**
 * Clase Vista
 * @author tania
 */
public class Vista {
    //Campos
    private JPanel panel1;
    JTabbedPane tabbedPane1;
    JFrame frame;
     //Doctor
     JTextField txtNombreDoctor;
     JTextField txtApellidosDoctor;
     JTextField txtNExpediente;
     DatePicker dateFechaNacimientoDoctor;
     JTextField txtEspecialidad;
     JButton btnAltaDoctor;
     JButton btnListarDoctores;
     JButton btnModificarDoctor;
     JButton btnEliminarDoctor;
     JList listDoctor;


     DefaultListModel dlmDoctor;



     //Especialidades
     JTextField txtNombreEspecialidad;
     JTextField txtAnosEspecialidad;
     DatePicker dateFechaTitulacion;
     JButton btnAltaEspecialidad;
     JButton btnModificarEspecialidad;
     JButton btnEliminarEspecialidad;
     JButton btnListarEspecialidad;
     JList listEspecialidad;

     DefaultListModel dlmEspecialidad;
        //Paciente
     JTextField txtNombrePaciente;
     JTextField txtApellidosPaciente;
     JTextField txtEdadPaciente;
     DatePicker dateFechaNacimientoPaciente;
     JButton btnAltaPaciente;
     JButton btnListarPaciente;
     JButton btnModificarPaciente;
     JButton btnEliminarPaciente;
     JList listPaciente;

     DefaultListModel dlmPaciente;
        //Operacion
     JTextField txtSala;
     JTextField txtDuracion;
     DatePicker dateFechaOperacion;
     JButton btnAltaOperacion;
     JButton btnListarOperaciones;
     JButton btnModificarOperacion;
     JButton btnEliminarOperacion;
     JList listOperacion;
     JList listPacientes;
     JButton btnListarPacientes;
     JList listOperacionPaciente;



     JTextField txtIdDoctor;
     JTextField txtIdEspecialidad;
     JTextField txtIdPaciente;
     JTextField txtIdOperacion;

    JTextField txtPacientes;
    DefaultComboBoxModel<Paciente> dcbPaciente;
    DefaultListModel dlmOperacion;
    DefaultListModel dlmPacientes;
    DefaultListModel dlmOperacionPaciente;


     //Menu
    JMenuItem salirItem;
    JMenuItem conexionItem;


    /**
     * Constructor Vista
     */
    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        crearMenu();
        crearModelo();
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
    }

    /**
     * Iniciar modelos
     */
    private void crearModelo() {
        //Doctor
        dlmDoctor=new DefaultListModel();
        listDoctor.setModel(dlmDoctor);

        //Especialidad
        dlmEspecialidad=new DefaultListModel();
        listEspecialidad.setModel(dlmEspecialidad);
        //Paciente
        dlmPaciente=new DefaultListModel();
        listPaciente.setModel(dlmPaciente);
        //Operacion
        dlmOperacion=new DefaultListModel();
        listOperacion.setModel(dlmOperacion);
        dlmPacientes=new DefaultListModel();
        listPacientes.setModel(dlmPacientes);
        dlmOperacionPaciente=new DefaultListModel();
        listOperacionPaciente.setModel(dlmOperacionPaciente);



    }

    /**
     * Crear menu
     */
    private void crearMenu() {
        JMenuBar barra=new JMenuBar();
        JMenu menu=new JMenu("Archivo");
        conexionItem=new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");
        salirItem=new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);

    }
}
