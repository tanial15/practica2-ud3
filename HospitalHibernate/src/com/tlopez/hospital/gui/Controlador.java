package com.tlopez.hospital.gui;

import com.tlopez.hospital.*;


import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase Controlador
 * @author tania
 * @see java.awt.event.ActionListener
 * @see javax.swing.event.ListSelectionListener
 */
public class Controlador implements ActionListener, ListSelectionListener {
    private Vista vista;
    private Modelo modelo;

    /**
     * Constructor del Controlador
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addListSelectionListener(this);
    }

    /**
     * Lista los doctores
     * @param doctors
     */
    private void listarDoctor(ArrayList<Doctor> doctors) {
        vista.dlmDoctor.clear();
        for(Doctor doctor : doctors){
            vista.dlmDoctor.addElement(doctor);
        }
    }

    /**
     * Lista las especialidades
     * @param especialidades
     */
    public void listarEspecialidades(ArrayList<Especialidades> especialidades){
        vista.dlmEspecialidad.clear();
        for(Especialidades especialidad : especialidades){
            vista.dlmEspecialidad.addElement(especialidad);
        }
    }

    /**
     * Lista las operaciones pacientes
     * @param operacions
     */
    public void listarOperacionPaciente(List<Operacion> operacions){
        vista.dlmOperacionPaciente.clear();
        for(Operacion operacion : operacions){
            vista.dlmOperacionPaciente.addElement(operacion);
        }
    }

    /**
     * Lista los pacientes de operacion
     * @param pacientes
     */
    public void listarPacientes(ArrayList<Paciente> pacientes){
        vista.dlmPacientes.clear();
        for(Paciente paciente : pacientes){
            vista.dlmPacientes.addElement(paciente);
        }
    }

    /**
     * Lista los pacientes
     * @param pacientes
     */
    public void listarPaciente(ArrayList<Paciente> pacientes){
        vista.dlmPaciente.clear();
        for(Paciente paciente : pacientes){
            vista.dlmPaciente.addElement(paciente);
        }
    }

    /**
     * Lista las operaciones
     * @param operacions
     */
    public void listarOperacion(ArrayList<Operacion> operacions){
        vista.dlmOperacion.clear();
        for(Operacion operacion : operacions){
            vista.dlmOperacion.addElement(operacion);
        }
    }
    /**
     * Método para añadir los ActionListener a los botones
     * @param listener
     */
    private void addActionListeners(ActionListener listener){
        vista.conexionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);

        vista.btnAltaDoctor.addActionListener(listener);
        vista.btnAltaEspecialidad.addActionListener(listener);
        vista.btnAltaPaciente.addActionListener(listener);
        vista.btnAltaOperacion.addActionListener(listener);

        vista.btnEliminarDoctor.addActionListener(listener);
        vista.btnEliminarEspecialidad.addActionListener(listener);
        vista.btnEliminarPaciente.addActionListener(listener);
        vista.btnEliminarOperacion.addActionListener(listener);

        vista.btnModificarDoctor.addActionListener(listener);
        vista.btnModificarEspecialidad.addActionListener(listener);
        vista.btnModificarOperacion.addActionListener(listener);
        vista.btnModificarPaciente.addActionListener(listener);

        vista.btnListarDoctores.addActionListener(listener);
        vista.btnListarPacientes.addActionListener(listener);
        vista.btnListarOperaciones.addActionListener(listener);
        vista.btnListarEspecialidad.addActionListener(listener);
        vista.btnListarPaciente.addActionListener(listener);
    }

    /**
     * Método para añadir los ListSelectionListener a los JLists
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener){
        vista.listDoctor.addListSelectionListener(listener);
        vista.listOperacion.addListSelectionListener(listener);
        vista.listPaciente.addListSelectionListener(listener);
        vista.listEspecialidad.addListSelectionListener(listener);
        vista.listOperacionPaciente.addListSelectionListener(listener);
        vista.listPacientes.addListSelectionListener(listener);
    }
    /**
     * Método que responde a un evento ActionEvent sobre los botones
     * @param e el evento lanzado
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;
            case "Conectar":
                vista.conexionItem.setEnabled(false);
                modelo.conectar();
                break;

            case "Alta Doctor":
                Doctor nuevoDoctor = new Doctor();
                nuevoDoctor.setNombre(vista.txtNombreDoctor.getText());
                nuevoDoctor.setApellidos(vista.txtApellidosDoctor.getText());
                nuevoDoctor.setFechaNac(Date.valueOf(vista.dateFechaNacimientoDoctor.getDate()));
                nuevoDoctor.setNumExpediente(Integer.parseInt(vista.txtNExpediente.getText()));
                modelo.altaDoctor(nuevoDoctor);
                listarDoctor(modelo.getDoctor());
                break;

            case "Listar Doctores":
                listarDoctor(modelo.getDoctor());
                break;

            case "Modificar Doctores":
                Doctor doctorSeleccion = (Doctor) vista.listDoctor.getSelectedValue();
                doctorSeleccion.setNombre(vista.txtNombreDoctor.getText());
                doctorSeleccion.setApellidos(vista.txtApellidosDoctor.getText());
                doctorSeleccion.setFechaNac(Date.valueOf(vista.dateFechaNacimientoDoctor.getDate()));
                doctorSeleccion.setNumExpediente(Integer.parseInt(vista.txtNExpediente.getText()));
                modelo.modificarDoctor(doctorSeleccion);
                listarDoctor(modelo.getDoctor());
                break;

            case "Eliminar Doctores":
                Doctor doctorBorrado  = (Doctor) vista.listDoctor.getSelectedValue();
                modelo.eliminarDoctor(doctorBorrado);
                listarDoctor(modelo.getDoctor());
                break;

            case "Alta Especialidad":
                Especialidades nuevaEspecialidad = new Especialidades();
                nuevaEspecialidad.setNombre(vista.txtNombreEspecialidad.getText());
                nuevaEspecialidad.setFechaTitulacion(Date.valueOf(vista.dateFechaTitulacion.getDate()));
                nuevaEspecialidad.setAnios(Integer.parseInt(vista.txtAnosEspecialidad.getText()));
                modelo.altaEspecialidad(nuevaEspecialidad);
                listarEspecialidades(modelo.getEspecialidad());
                break;

            case "Listar Especialidad":
                listarEspecialidades(modelo.getEspecialidad());
                break;

            case "Modificar Especialidad":
                Especialidades especialidadesSeleccion = (Especialidades) vista.listEspecialidad.getSelectedValue();
                especialidadesSeleccion.setNombre(vista.txtNombreEspecialidad.getText());
                especialidadesSeleccion.setFechaTitulacion(Date.valueOf(vista.dateFechaTitulacion.getDate()));
                especialidadesSeleccion.setAnios(Integer.parseInt(vista.txtAnosEspecialidad.getText()));
                modelo.modificarEspecialidad(especialidadesSeleccion);
                listarEspecialidades(modelo.getEspecialidad());
                break;

            case "Eliminar Especialidad":
                Especialidades especialidadesBorrado  = (Especialidades) vista.listEspecialidad.getSelectedValue();
                modelo.eliminarEspecialidad(especialidadesBorrado);
                listarEspecialidades(modelo.getEspecialidad());
                break;
            case "Alta Paciente":
                Paciente nuevoPaciente = new Paciente();
                nuevoPaciente.setNombre(vista.txtNombrePaciente.getText());
                nuevoPaciente.setApellidos(vista.txtApellidosPaciente.getText());
                nuevoPaciente.setFechaNac(Date.valueOf(vista.dateFechaNacimientoPaciente.getDate()));
                nuevoPaciente.setEdad(Integer.parseInt(vista.txtEdadPaciente.getText()));
                modelo.altaPaciente(nuevoPaciente);
                listarPaciente(modelo.getPaciente());

                break;

            case "Listar Paciente":
                listarPaciente(modelo.getPacientes());
                break;

            case "Modificar Pacientes":
                Paciente pacienteSeleccion = (Paciente) vista.listPaciente.getSelectedValue();
                pacienteSeleccion.setNombre(vista.txtNombrePaciente.getText());
                pacienteSeleccion.setApellidos(vista.txtApellidosPaciente.getText());
                pacienteSeleccion.setFechaNac(Date.valueOf(vista.dateFechaNacimientoPaciente.getDate()));
                pacienteSeleccion.setEdad(Integer.parseInt(vista.txtEdadPaciente.getText()));
                modelo.modificarPaciente(pacienteSeleccion);
                listarPaciente(modelo.getPaciente());
                break;

            case "Eliminar Pacientes":
                Paciente pacienteBorrado  = (Paciente) vista.listPaciente.getSelectedValue();
                modelo.eliminarPaciente(pacienteBorrado);
                listarPaciente(modelo.getPaciente());
                break;
            case "Alta Operacion":
                Operacion nuevoOperacion = new Operacion();
                nuevoOperacion.setSala(vista.txtSala.getText());
                nuevoOperacion.setDuracion(Integer.parseInt(vista.txtDuracion.getText()));
                nuevoOperacion.setFecha(Date.valueOf(vista.dateFechaOperacion.getDate()));
                nuevoOperacion.setPaciente((Paciente) vista.dcbPaciente.getSelectedItem());

                modelo.altaOperacion(nuevoOperacion);
                listarOperacion(modelo.getOperacion());
                break;

            case "Listar Operaciones":
                listarOperacion(modelo.getOperacion());
                break;

            case "Modificar Operaciones":
                Operacion operacionSeleccion = (Operacion) vista.listOperacion.getSelectedValue();
                operacionSeleccion.setSala(vista.txtSala.getText());
                operacionSeleccion.setDuracion(Integer.parseInt(vista.txtDuracion.getText()));
                operacionSeleccion.setFecha(Date.valueOf(vista.dateFechaOperacion.getDate()));
                operacionSeleccion.setPaciente((Paciente) vista.listPacientes.getSelectedValue());

                System.out.println(vista.listPacientes.getSelectedValue());
                modelo.modificarOperacion(operacionSeleccion);
                listarOperacion(modelo.getOperacion());
                break;
            case "Eliminar Operaciones":
                Operacion operacionBorrado  = (Operacion) vista.listOperacion.getSelectedValue();
                modelo.eliminarOperacion(operacionBorrado);
                listarOperacion(modelo.getOperacion());
                break;
            case "Listar Pacientes":
                listarPacientes(modelo.getPaciente());

                break;


        }



    }
    /**
     * Metodo que responde a las selecciones en los JLists
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()) {
            if (e.getSource() == vista.listDoctor) {
                Doctor doctorSeleccion = (Doctor) vista.listDoctor.getSelectedValue();
                vista.txtIdDoctor.setText(String.valueOf(doctorSeleccion.getId()));
                vista.txtNombreDoctor.setText(doctorSeleccion.getNombre());
                vista.txtApellidosDoctor.setText(doctorSeleccion.getApellidos());
                vista.dateFechaNacimientoDoctor.setDate(doctorSeleccion.getFechaNac().toLocalDate());
                vista.txtNExpediente.setText(String.valueOf(doctorSeleccion.getNumExpediente()));

            }

            if (e.getSource() == vista.listEspecialidad) {
                Especialidades especialidadesSeleccion = (Especialidades) vista.listEspecialidad.getSelectedValue();
                vista.txtIdEspecialidad.setText(String.valueOf(especialidadesSeleccion.getId()));
                vista.txtNombreEspecialidad.setText(especialidadesSeleccion.getNombre());
                vista.dateFechaTitulacion.setDate(especialidadesSeleccion.getFechaTitulacion().toLocalDate());
                vista.txtAnosEspecialidad.setText(String.valueOf(especialidadesSeleccion.getAnios()));
            }
            if (e.getSource() == vista.listPaciente) {
                Paciente pacienteSeleccion = (Paciente) vista.listPaciente.getSelectedValue();
                vista.txtIdPaciente.setText(String.valueOf(pacienteSeleccion.getId()));
                vista.txtNombrePaciente.setText(pacienteSeleccion.getNombre());
                vista.txtApellidosPaciente.setText(pacienteSeleccion.getApellidos());
                vista.dateFechaNacimientoPaciente.setDate(pacienteSeleccion.getFechaNac().toLocalDate());
                vista.txtEdadPaciente.setText(String.valueOf(pacienteSeleccion.getEdad()));

                if(e.getSource() == vista.listPacientes){
                    Paciente pacienteSeleccionado = (Paciente) vista.listPacientes.getSelectedValue();
                    listarOperacionPaciente(modelo.getOperacionPaciente(pacienteSeleccionado));
                }
            }

            if (e.getSource() == vista.listOperacion) {
                Operacion operacionSeleccion = (Operacion) vista.listOperacion.getSelectedValue();
                vista.txtIdOperacion.setText(String.valueOf(operacionSeleccion.getId()));
                vista.txtSala.setText(operacionSeleccion.getSala());
                vista.txtDuracion.setText(String.valueOf(operacionSeleccion.getDuracion()));
                vista.dateFechaOperacion.setDate(operacionSeleccion.getFecha().toLocalDate());

                if (operacionSeleccion.getPaciente() != null) {
                    vista.txtPacientes.setText(String.valueOf(operacionSeleccion.getPaciente().toString()));
                } else {
                    vista.txtPacientes.setText("");
                }
            }

        }else{
               if(e.getSource() == vista.listPacientes){
                   Paciente pacienteSeleccionado = (Paciente) vista.listPacientes.getSelectedValue();
                   listarOperacionPaciente(modelo.getOperacionPaciente(pacienteSeleccionado));
               }


           }


    }
}
