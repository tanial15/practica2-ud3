package com.tlopez.hospital;


import com.tlopez.hospital.gui.Controlador;
import com.tlopez.hospital.gui.Modelo;
import com.tlopez.hospital.gui.Vista;

public class Main {
   public static void main(String[] args) {
      Vista vista=new Vista();
      Modelo modelo=new Modelo();
      Controlador controlador=new Controlador(vista,modelo);
   }

}