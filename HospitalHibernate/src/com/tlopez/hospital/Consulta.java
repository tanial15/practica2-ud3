package com.tlopez.hospital;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "doctores_pacientes", schema = "hospital")
public class Consulta {
    private String diagnostico;
    private Date fecha;
    private int id;
    private Paciente paciente;
    private Doctor doctor;

    @Basic
    @Column(name = "diagnostico")
    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    @Basic
    @Column(name = "fecha")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Consulta consulta = (Consulta) o;
        return id == consulta.id &&
                Objects.equals(diagnostico, consulta.diagnostico) &&
                Objects.equals(fecha, consulta.fecha);
    }

    @Override
    public int hashCode() {
        return Objects.hash(diagnostico, fecha, id);
    }

    @ManyToOne
    @JoinColumn(name = "id_paciente", referencedColumnName = "id", nullable = false)
    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    @ManyToOne
    @JoinColumn(name = "id_doctor", referencedColumnName = "id", nullable = false)
    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Consulta() {
    }

    public Consulta(String diagnostico, Date fecha) {
        this.diagnostico = diagnostico;
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return diagnostico+ ", " + fecha ;
    }
}
